window.appState = {
    maxGforce: 0
}

function init() {
    if('DeviceMotionEvent' in window) {
        window.addEventListener('devicemotion', deviceMotionHandler, false);
    }
}

function deviceMotionHandler(eventData) {
    var acceleration = eventData.acceleration;
    const x = acceleration.x;
    const y = acceleration.y;
    const z = acceleration.z;
    const gforce = Math.sqrt(x*x + y*y + z*z).toFixed(3);
    
    document.getElementById("meter").style.height = gforce * 20 + 'px';

    document.getElementById("gforce").innerHTML = gforce;
    if(gforce > window.appState.maxGforce) {
        window.appState.maxGforce = gforce;
        document.getElementById("max-gforce").innerHTML = gforce;
    }
}

window.onload = init;