class SeasonFinder {
    static determineSeason(latitude) {
        const isBelowEquator = latitude < 0;
        const today = new Date();
        let currentMonth = today.getMonth();
        const currentDay = today.getDate();
        const seasons = ['winter', 'spring', 'summer', 'autumn'];
        // Seasons start on the 22nd of the month. Thus if the day is >= 22nd, we should use the next month to determine the season.
        if(currentDay >= 22) currentMonth++;
        let currentSeasonIndex = Math.floor(currentMonth / 3);
        // Locations below the equator have opposite seasons. Thus for these we skip two seasons.
        if(isBelowEquator) currentSeasonIndex = (currentSeasonIndex + 2) % 4;
        
        return seasons[currentSeasonIndex];
    }
}

export default SeasonFinder;