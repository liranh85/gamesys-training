'use strict';

import SeasonFinder from './SeasonFinder.js';

(function() {
    const seasonNode = document.getElementById('season');

    function success(pos) {
        hideLoader();
        const currentSeason = SeasonFinder.determineSeason(pos.coords.latitude);
        seasonNode.setAttribute('data-season', currentSeason);
    }

    function error(err) {
        console.warn(`ERROR(${err.code}): ${err.message}`);
        hideLoader();
        seasonNode.innerHTML = 'Failed to retrieve your location. Please consider moving to a more open space and reloading the page.';
    }

    function showLoader() {
        document.getElementById('loader').style.display = 'block';
    }

    function hideLoader() {
        document.getElementById('loader').style.display = 'none';
    }

    function init() {
        const options = {
            timeout: 10000
        };
        showLoader();
        navigator.geolocation.getCurrentPosition(success, error, options);
    }

    window.onload = init;
})();