window.onload = function() {
	var targetDate = nextDay(5);
	targetDate.setHours(18, 0, 0);
	var secondsLeft = secondsTillDate(targetDate);
	// console.log(secondsLeft);
	var countdown = document.getElementById("countdown");
	countdown.setAttribute("data-seconds", secondsLeft);
	countdown.classList.add("counting");
	countDownTime(countdown);
}

function secondsTillDate(targetDate) {
	// I need to change this to take a day, and find its NEXT occurence at any point in time.
	var now = new Date();
	var fromEpochToToday = now.getTime();
	// console.log(Math.floor(fromEpochToToday/1000));
	var fromEpochToTargetDate = targetDate.getTime();
	console.log(Math.floor(fromEpochToTargetDate/1000));

	var msTillTargetDate = fromEpochToTargetDate - fromEpochToToday;
	var secTillTargetDate = Math.floor(msTillTargetDate / 1000);
	return secTillTargetDate;
}

// http://stackoverflow.com/questions/1579010/get-next-date-from-weekday-in-javascript
function nextDay(x){
    var now = new Date();    
    now.setDate(now.getDate() + (x+(7-now.getDay())) % 7);
    return now;
}

function countDownTime(elm) {
	setInterval(() => {
		var secondsLeft = parseInt(elm.getAttribute("data-seconds"));
		
		if (secondsLeft <= 0) {
			clearInterval(this);
			timeElapsed(elm);
			return;
		}

		// Decrease seconds by 1 in here
		elm.setAttribute("data-seconds", secondsLeft - 1);
	}, 1000)
}

function timeElapsed(elm) {
	elm.innerHTML = "The weekend is here!";
	elm.classList.remove("counting");
	elm.classList.add("done");
}