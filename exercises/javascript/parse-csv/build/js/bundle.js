(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CSVParser = function () {
	function CSVParser(csvPath) {
		_classCallCheck(this, CSVParser);

		this.csvPath = csvPath;
	}

	_createClass(CSVParser, [{
		key: "parse",
		value: function parse() {
			var _this = this;

			return new Promise(function (resolve, reject) {
				_this.get(_this.csvPath).then(function (response) {

					var allLines = response.split(";");
					var headers = allLines.shift().split(",");
					// Remove last line, as it's empty
					allLines.pop();
					_this.parsedData = [];

					for (var i = 0; i < allLines.length; i++) {
						var lineData = allLines[i].split(",");
						var lineObj = {};

						// Auto generated transaction ID
						lineObj.transactionId = i + 1;
						// Transaction_date
						lineObj[headers[0]] = new Date(lineData[0]);
						// Product
						lineObj[headers[1]] = lineData[1];
						// Price
						lineObj[headers[2]] = _this.formatAsPrice(lineData[2]);
						// Payment_Type
						lineObj[headers[3]] = lineData[3];
						// Name
						lineObj[headers[4]] = _this.getFirstWordCapitalised(lineData[4]);
						// City
						lineObj[headers[5]] = lineData[5];
						// State
						lineObj[headers[6]] = lineData[6];
						// Country
						lineObj[headers[7]] = lineData[7];
						// Account_Created
						lineObj[headers[8]] = lineData[8];
						// Last_Login
						lineObj[headers[9]] = lineData[9];
						// Latitude
						lineObj[headers[10]] = Number(lineData[10]).toFixed(4);
						// Longitude
						lineObj[headers[11]] = Number(lineData[11]).toFixed(4);

						_this.parsedData.push(lineObj);
					}

					resolve();
				}, function (error) {
					console.error("Failed!", error);
				});
			});
		}
	}, {
		key: "get",
		value: function get(url) {
			return new Promise(function (resolve, reject) {
				var req = new XMLHttpRequest();
				req.open('GET', url);

				req.onload = function () {
					if (req.status === 200) {
						resolve(req.response);
					} else {
						reject(Error(req.statusText));
					}
				};

				req.onerror = function () {
					reject(Error("Network Error"));
				};

				req.send();
			});
		}
	}, {
		key: "formatAsPrice",
		value: function formatAsPrice(number) {
			var currency = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "£";

			var numArr = number.toString().split("");

			for (var i = numArr.length - 3; i > 0; i -= 3) {
				numArr.splice(i, 0, ",");
			}
			numArr.unshift(currency);
			return numArr.join("");
		}
	}, {
		key: "getFirstWordCapitalised",
		value: function getFirstWordCapitalised(str) {
			var firstWord = str.split(" ")[0];
			return firstWord.charAt(0).toUpperCase() + firstWord.slice(1);
		}
	}, {
		key: "getData",
		value: function getData(criteria) {
			return this.findData(criteria, false);
		}
	}, {
		key: "filterData",
		value: function filterData(criteria) {
			return this.findData(criteria);
		}
	}, {
		key: "findData",
		value: function findData(query) {
			var meetAllCriteria = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

			// If no criteria was given, return all data
			if (typeof query === "undefined") {
				return this.parsedData;
			}

			var queryProps = Object.keys(query);
			var result = [];

			// If we need to meet all the criteria, return array with entries in which ALL criteria matched the record's corresponding properties
			if (meetAllCriteria) {
				return this.parsedData.filter(function (entry) {
					return queryProps.every(function (prop) {
						return entry[prop] === query[prop];
					});
				});
			}
			// Otherwise, return array with entries in which SOME criteria matched
			return this.parsedData.filter(function (entry) {
				return queryProps.some(function (prop) {
					return entry[prop] === query[prop];
				});
			});
		}
	}, {
		key: "getTransactionsFrom",
		value: function getTransactionsFrom(date) {
			return this.parsedData.filter(function (entry) {
				return entry['Transaction_date'] > date;
			});
		}
	}, {
		key: "getTransactionsBefore",
		value: function getTransactionsBefore(date) {
			return this.parsedData.filter(function (entry) {
				return entry['Transaction_date'] < date;
			});
		}
	}]);

	return CSVParser;
}();

exports.default = CSVParser;

},{}],2:[function(require,module,exports){
"use strict";

var _CSVParser = require("./CSVParser.js");

var _CSVParser2 = _interopRequireDefault(_CSVParser);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var csvPath = "../SalesJan2009.csv";
var parser = new _CSVParser2.default(csvPath);
parser.parse().then(function () {
	// console.log(parser.filterData({
	// 	// 'Last_Login': '1/3/09 14:22',
	// 	Payment_Type: 'Amex',
	// 	Country: 'United States'
	// }));
	console.log(parser.getTransactionsFrom(new Date(2009, 0, 13)));
});

},{"./CSVParser.js":1}]},{},[2]);
