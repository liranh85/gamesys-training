class CSVParser {
	constructor(csvPath) {
		this.csvPath = csvPath;
	}

	parse() {
		return new Promise((resolve, reject) => {
			this.get(this.csvPath).then(response => {
				
				let allLines = response.split(";");
				let headers = allLines.shift().split(",");
				// Remove last line, as it's empty
				allLines.pop();
				this.parsedData = [];

				for(let i = 0; i < allLines.length; i++) {
					let lineData = allLines[i].split(",");
					let lineObj = {};
					
					// Auto generated transaction ID
					lineObj.transactionId = i + 1;
					// Transaction_date
					lineObj[headers[0]] = new Date(lineData[0]);
					// Product
					lineObj[headers[1]] = lineData[1];
					// Price
					lineObj[headers[2]] = this.formatAsPrice(lineData[2]);
					// Payment_Type
					lineObj[headers[3]] = lineData[3];
					// Name
					lineObj[headers[4]] = this.getFirstWordCapitalised(lineData[4]);
					// City
					lineObj[headers[5]] = lineData[5];
					// State
					lineObj[headers[6]] = lineData[6];
					// Country
					lineObj[headers[7]] = lineData[7];
					// Account_Created
					lineObj[headers[8]] = lineData[8];
					// Last_Login
					lineObj[headers[9]] = lineData[9];
					// Latitude
					lineObj[headers[10]] = Number(lineData[10]).toFixed(4);
					// Longitude
					lineObj[headers[11]] = Number(lineData[11]).toFixed(4);

					this.parsedData.push(lineObj);
				}

				resolve();

			}, error => {
				console.error("Failed!", error);
			});
		})
	}

	get(url) {
	    return new Promise((resolve, reject) => {
	        const req = new XMLHttpRequest();
	        req.open('GET', url);

	        req.onload = () => {
	            if (req.status === 200) {
	                resolve(req.response);
	            } else {
	                reject(Error(req.statusText));
	            }
	        };

	        req.onerror = () => {
	            reject(Error("Network Error"));
	        };

	        req.send();
	    });
	}

	formatAsPrice(number, currency = "£") {
		let numArr = number.toString().split("");

		for (let i = numArr.length - 3; i > 0 ; i -= 3) {
			numArr.splice(i, 0, ",");
		}
		numArr.unshift(currency);
		return numArr.join("");
	}

	getFirstWordCapitalised(str) {
		let firstWord = str.split(" ")[0];
		return firstWord.charAt(0).toUpperCase() + firstWord.slice(1);
	}
	
	getData(criteria) {
		return this.findData(criteria, false);
	}
	
	filterData(criteria) {
		return this.findData(criteria);
	}

	findData(query, meetAllCriteria = true) {
		// If no criteria was given, return all data
		if(typeof query === "undefined") {
			return this.parsedData;
		}

		const queryProps = Object.keys(query);
		let result = [];

		// If we need to meet all the criteria, return array with entries in which ALL criteria matched the record's corresponding properties
		if (meetAllCriteria) {
			return this.parsedData.filter(entry => queryProps.every(prop => entry[prop] === query[prop]));
		}
		// Otherwise, return array with entries in which SOME criteria matched
		return this.parsedData.filter(entry => queryProps.some(prop => entry[prop] === query[prop]));
	}
	
	getTransactionsFrom(date) {
		return this.parsedData.filter(entry => entry['Transaction_date'] > date);
	}

	getTransactionsBefore(date) {
		return this.parsedData.filter(entry => entry['Transaction_date'] < date);
	}
}

export default CSVParser;