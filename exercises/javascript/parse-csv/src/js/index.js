import CSVParser from "./CSVParser.js";

let csvPath = "../SalesJan2009.csv";
let parser = new CSVParser(csvPath);
parser.parse().then(() => {
	// console.log(parser.filterData({
	// 	// 'Last_Login': '1/3/09 14:22',
	// 	Payment_Type: 'Amex',
	// 	Country: 'United States'
	// }));
	console.log(parser.getTransactionsFrom(new Date(2009, 0, 13)));
});