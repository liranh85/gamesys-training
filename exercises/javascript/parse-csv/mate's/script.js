// 6. Create a function that takes a CSV file (attached is a sample) as a string and parses it in to an array of objects. The object will have property names from the first line of the CSV and values are the values from each line. When parsing the data make sure:
//  - dates are parsed to actual JS Dates
//  - lat and long values rounded to 4 decimal points
//  - names are capitalised (change from all caps or all lowercase)
//  - names only use the first name when two names are provided
//  - you add an extra id property that is just an incremented unique number.
// Then create some functions to retrieve the data:
//  - getData(property, value) and filterData(property, value): both return an array of object results from the already parsed data. Make sure they work with an array of [property, value] pairs. The difference in behaviour is that getData returns all data matching any of your requirements while filterData only returns data matching ALL you requirements the same time. (Think about OR and AND combinators).
//  - getFormattedPrice(id): returns a formatted price, like £1,254
//  - getTransactionsFrom(Date), getTransactionsBefore(Date): returns array of data from/before date.
// Make sure you don’t write duplicate code when doing the similarly behaving methods. (2 hours)

class CSVData {
	constructor(settings) {
		this.parsedData = [];
		this.priceSeparator = settings.priceSeparator;
		this.currencySign = settings.currencySign;
	}

	getTransactionsFrom(date) {

		// Return transactions from specific date
		return this.parsedData.filter(entry => entry['Transaction_date'].getDate() > date.getDate());
	}

	getTransactionsBefore(date) {

		// Return transactions before specific date
		return this.parsedData.filter(entry => entry['Transaction_date'].getDate() < date.getDate());
	}

	getFormattedPrice(query) {
		const entry = this.getData(query)[0];
		let amount = entry['Price'].toString();

		// Insert separator at given position
		const insertSeparator = (string, position) => [string.slice(0, position), this.priceSeparator, string.slice(position)].join('');

		// Iterate to every third character from the end then insert separator
		for (let i = amount.length; i > 0; i = i - 3) {
			amount = i !== amount.length ? insertSeparator(amount, i) : amount;
		}

		return this.currencySign + amount;
	}

	retrieveData(method, query) {
		const queryProps = query ? Object.keys(query) : [];
		let result = [];

		// If we receive query filter down results
		if (queryProps.length > 0) {

			if (method === 'get') {

				// Return result if at least one property matched in entry
				result = this.parsedData.filter(entry => queryProps.some(prop => entry[prop] === query[prop]));
			}

			if (method === 'filter') {

				// Return results if all properties are matched in entry
				result = this.parsedData.filter(entry => queryProps.every(prop => entry[prop] === query[prop]));
			}

		// Otherwise return all data
		} else {
			result = this.parsedData;
		}

		return result;
	}

	getData(query) {
		return this.retrieveData('get', query);
	}

	filterData(query) {
		return this.retrieveData('filter', query);
	}

	formatData(data) {
		return data.map((entry, index) => {

			const capitalise = string => string.split(' ').map( word => word[0].toUpperCase() + word.slice(1) ).join(' ');
			const trimName = string => string.split(' ')[0];

			// Formate already parsed data according to our custom rules
			return {
				
				// Parse dates into javascript Date
				'Account_Created': new Date( entry['Account_Created'] ),
				'Transaction_date': new Date( entry['Transaction_date']),
				'Last_Login': new Date( entry['Last_Login']),

				// Capitalise names
				'City': capitalise( entry['City'] ),
				'Country': capitalise( entry['City'] ),
				'Name': capitalise( trimName( entry['Name'] ) ),
				
				// Format numbers
				'Latitude': parseFloat(entry['Latitude']).toFixed(4),
				'Longitude': parseFloat(entry['Longitude']).toFixed(4),
				'Price': parseFloat(entry['Price']),
				
				// Leave as is
				'Payment_Type': entry['Payment_Type'],
				'Product': entry['Product'],
				'State': entry['State'],
				
				// Assign ID for entry
				'Id': index,
			};
		});
	}

	parseCSV(rawData) {

		const parser = new Promise(() => {
			
			// Split data into rows
			const rows = rawData.split(';');

			// Remove header from first raw and save into variable
			const header = rows.shift();

			// Split properties from header into array
			const props = header.split(',');

			// Remove last empty entry
			rows.pop();

			// Generate array of objects by iterating through each row
			let result = rows.map(row => {
				let dataObject = {};
				let data = row.split(',');
				
				// Save value from data to each property from the header
				props.forEach((prop, index) => {
					dataObject[prop] = data[index];
				});

				return dataObject;
			});

			// Format the resulting data
			this.parsedData = this.formatData(result);

			// If data has content then resolve the promise otherwise throw error
			if (this.parsedData.length > 0) {
				resolve(this.parsedData);
			} else {
				reject("Error: Failed parsing CSV.");
			}
		});

		return parser;
	}

	loadCSV(fileName) {

		// Load file and start parsing data then return promise
		const asyncLoad = new Promise((resolve, reject) => {
			const req = new XMLHttpRequest();
			req.addEventListener('load', event => {
				resolve(event.target.responseText)
			});
			req.open('GET', fileName);
			req.send();
		});
		
		return asyncLoad;
	}
}

function init() {

	// Initialise new CSVData class
	const myData = new CSVData({
		priceSeparator: ',',
		currencySign: '£'
	});

	// Load CSV then test features
	myData
		.loadCSV('SalesJan2009.csv')
		.then(myData.parseCSV)
		.then(() => {
			
			const targetDay = new Date(2009, 01, 15);

			console.log( myData.getData());
			console.log( myData.getData({'City': 'London', 'Payment_Type': 'Visa'}));
			console.log( myData.filterData({'City': 'London', 'Payment_Type': 'Visa'}));

			console.log( myData.getFormattedPrice({'Id': 128}));

			console.log( myData.getTransactionsFrom(targetDay));
			console.log( myData.getTransactionsBefore(targetDay));
		});
}

window.onload = init;
