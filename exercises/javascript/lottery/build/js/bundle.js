(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var LotteryMachine = function () {
	function LotteryMachine(settings) {
		_classCallCheck(this, LotteryMachine);

		Object.assign(this, settings);
	}

	_createClass(LotteryMachine, [{
		key: 'drawNumbers',
		value: function drawNumbers() {
			var _this = this;

			var randomNums = Array(this.num).fill(null);

			if (typeof this.num === 'undefined') {
				return false;
			}
			this.min = typeof this.min === 'undefined' ? 1 : this.min;
			this.max = typeof this.max === 'undefined' ? 100 : this.max;

			randomNums = randomNums.map(function (el, index, arr) {
				var randomNum = void 0;
				// While new randomNum already exists in the randomNums array, genereate random numbers
				do {
					randomNum = Math.floor(Math.random() * _this.max + _this.min);
				} while (arr.includes(randomNum));

				// Once a random number that is not yet on the randomNums array has been found, add it to the array.
				return randomNum;
			});

			return randomNums;
		}
	}]);

	return LotteryMachine;
}();

exports.default = LotteryMachine;

/* NOTES AND QUESTIONS:
	- Can't use .map for the randomNums, as I don't have access to the array at each iteration.
	- Can we use array.includes yet? Or not good idea due to poor browser support?
	- When the JS is transpiled and bundled, is there any way to view it in its original form in the browser, and to see where errors came from?
 */

},{}],2:[function(require,module,exports){
"use strict";

var _LotteryMachine = require("./LotteryMachine.js");

var _LotteryMachine2 = _interopRequireDefault(_LotteryMachine);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function init() {
	var lotteryMachine = new _LotteryMachine2.default({
		num: 5,
		min: 1,
		max: 50
	});
	var lotteryNumbers = lotteryMachine.drawNumbers();
	var balls = document.getElementsByClassName("ball");

	for (var i = 0; i < balls.length; i++) {
		balls[i].setAttribute("data-number", lotteryNumbers[i]);
		// debugger;
		balls[i].style.backgroundColor = "hsl(" + 360 / 50 * lotteryNumbers[i] + ", 75%, 50%)";
		// balls[i].style.backgroundColor = "hsl(2, 75%, 50%)";
	}
}

window.onload = init;

},{"./LotteryMachine.js":1}]},{},[2]);
