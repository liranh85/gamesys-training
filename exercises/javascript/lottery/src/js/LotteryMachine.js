class LotteryMachine {
	constructor(settings) {
		Object.assign(this, settings);
	}

	drawNumbers() {
		let randomNums = Array(this.num).fill(null);

		if(typeof this.num === 'undefined') {
			return false;
		}
		this.min = (typeof this.min === 'undefined' ? 1 : this.min);
		this.max = (typeof this.max === 'undefined' ? 100 : this.max);

		randomNums = randomNums.map((el, index, arr) => {
			let randomNum;
			// While new randomNum already exists in the randomNums array, genereate random numbers
			do {
				randomNum = Math.floor( (Math.random() * this.max) + this.min);
			} while(arr.includes(randomNum));
			
			// Once a random number that is not yet on the randomNums array has been found, add it to the array.
			return randomNum;
		});

		return randomNums;
	}
}

export default LotteryMachine;

/* NOTES AND QUESTIONS:
	- Can we use array.includes yet? Or not good idea due to poor browser support? => yes, with polyfill which can be taken care of by the gulpscript.
	- When the JS is transpiled and bundled, is there any way to view it in its original form in the browser, and to see where errors came from?
 */