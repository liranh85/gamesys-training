import LotteryMachine from './LotteryMachine.js';

function init() {
	let lotteryMachine = new LotteryMachine({
		num: 5,
		min: 1,
		max: 50
	});
	let lotteryNumbers = lotteryMachine.drawNumbers();
	var balls = document.getElementsByClassName("ball");
	
	for (var i = 0; i < balls.length; i++) {
		balls[i].setAttribute("data-number", lotteryNumbers[i]);
		// debugger;
		balls[i].style.backgroundColor = "hsl(" + 360/50 * lotteryNumbers[i] + ", 75%, 50%)";
		// balls[i].style.backgroundColor = "hsl(2, 75%, 50%)";
	}	
}

window.onload = init;